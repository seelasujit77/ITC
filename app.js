var express    = require('express'),
    app        = express(),
    mongodb = require('./db'),
    bodyParser = require('body-parser'),
    port = process.env.PORT ||  8080,
     _ =require('underscore'),      

//    port = process.env.PORT ||  3000,
    Globals = require('./globals'),
    log4js = require('log4js');
    
    

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

app.use(function(req,res,next){
  
  res.header('Access-Control-Allow-Origin', '*'); // We can access from anywhere
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    next();
});

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logResponseBody);




function logResponseBody(req, res, next) {
    var oldWrite = res.write,
        oldEnd = res.end,
        chunks = [],
        t1 = new Date();

    res.write = function (chunk) {
        chunks.push(chunk);
        oldWrite.apply(res, arguments);
    };

    res.end = function (chunk) {
        if (chunk)
            chunks.push(chunk);
        var t2 = new Date();
        
        oldEnd.apply(res, arguments);
    };

    next();
};


process.on('SIGINT', function() {
    mongodb.close(function(){
        logger.info('closing db');
        process.exit(0);
    });
});

process.on('uncaughtException', function(err) {
    // handle the error safely
    logger.info(err.stack);
});

mongodb.connect(Globals.MongoHost, Globals.MongoPort, Globals.MongoDB, function(err){
    if(err){
        logger.info("Problem in connecting MongoDB.");
    }else{
        logger.info("Connected to MongoDB.");
        app.listen(port, function () {
            logger.info('API\'s work at http://localhost:' + port + " url.");
            //setTimeout(scheduler, Globals.graceTime);  // uncomment to track the ITC devices on and off status
        });
      
    }
});

app.get('/', function(req, res) {
    res.sendFile(Globals.appRoot + '/public/views/layout.html');
});


// dash board  apis 

 
  // filing the  data into the  dash baord 

   app.get('/userslistcount',function(req,res){
   

     mongodb.findAll("itcdevices",function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result.length); 

         }

     });
   
   
   });

  app.get('/userslist',function(req,res){
   

     mongodb.findAll("itcdevices",function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result); 

         }

     });
   
   
   });


app.get('/usersActivelistcount',function(req,res){
   
   
   mongodb.findByObjects("itcdevices",{itcStatus:"Active"},function(err,result)
     {
       
        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result.length); 

         }
             
       

      });
   
});

app.get('/usersInactivelistcount',function(req,res){
   
  mongodb.findByObjects("itcdevices",{itcStatus:"InActive"},function(err,result)
     {
       
        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result.length); 

         }
             
       

      });
   
});

app.get('/usersUnlocklistcount',function(req,res){
   
  mongodb.findByObjects("devicelist",{shsfullpayment:1},function(err,result)
     {
       
        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             res.json(result.length); 

         }
             
       

      });
   
});


// Device  dashboard 

// getting the device  list 

app.get('/adddevice',function(req,res){
    
    
     mongodb.findAll("itcdevices",function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

             // console.log(""+JSON.stringify(result));
             res.json(result); 
            
         }

     });
   
    
});

// removing the  devices from the list 

app.delete('/deletedevice/:id',function(req,res){
    var id=req.params.id;
  
    mongodb.deleteOneByID("itcdevices",id,function(err,result){

        if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{   
             res.json(result);    
         }

     });
});

// editing the devices for the list 

app.get('/devicelistdata/:id',function(req,res)
{
    var id=req.params.id;
    

   // logger.error("app js id"+id);

    mongodb.findById("itcdevices",id,function(err,result){

        if(err)
         {
            //console.log("err"+err);
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{   
         // console.log(""+JSON.stringify(result));
             res.json(result);    
         }

     });



});


app.post('/addLight',function(req,res){

    mongodb.save('itclights',req.body,function(err,res){

      if(err){
           res.send({"status":0,"message":"error in inserting data"});
      }
      else
      {
          res.send({"status":1,"message":"Data saved Sucessfully"});
      }

    });
});


app.get('/getLights',function(req,res){

     mongodb.findByObjects("itclights",{},function(err,datares){

         if(err){
           res.send({"status":0,"message":"error in retriving  data"});
      }
      else
      {
           res.json(datares);
      }
     });
});





// adding the device 

app.post('/adddevice',function(req,res){
    
   // console.log(""+JSON.stringify(req.body));
      
    
    /**/
 
      mongodb.findByObjects("itcdevices",{"itcSno":req.body.itcSno},function(err,r){

       if(err)
       {

       }
       else
       {
          if(r.length>0)
          {
               console.log("shs number already exits");
              res.send({"status":0,"message":"shs number already exit ,please try with new shs"});
          }
          else
          {

              mongodb.save('itcdevices',req.body, function(err, result)
                {
                        if(err)
                        {

                            res.send({"status":0,"message":"error in insertion of the record"});
                        }
                        else
                        {

                           res.json(result); 
                        }

                        
                  }  );

          }
       }

      });



    
});



// api  for device level

app.post('/getEvent',function(req,res){
    
    //console.log(""+JSON.stringify(req.body));
    mongodb.findByObject('itcdevices',{"itcSno":req.body.S_ID},function(err,edata)
     {

         if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

              if(edata!=null)
              {
               res.json(resobj); 
             
              }
              else
              {
                   res.send({"status":0,"message":"no data found of given shsnumber please try again with valid shsnumber"});
              }
            

         }


     } );
   


});

app.post('/setStatus',function(req,res){

    logger.info(""+JSON.stringify(req.body));
      
      // update the latest values  to the device list and add to the setstaatus 
//   var shssendstatus;
//        if(req.body.itcStatus==="1")
//        {
//         shssendstatus="Active";
//        }
//        else
//        {
//         shssendstatus="InActive";
//        }

     var objc={

           "itclatitude":req.body.shslatitude,
           "itclongitude":req.body.shslongitude,
           "V_Batt":req.body.V_Batt,
           "I_Load":req.body.I_Load,
           "V_Load":req.body.V_Load,
           "State":req.body.Chsts,
           "itcStatus":"Active",
           "sid":parseInt(req.body.S_ID),
           "itcstatusdate":new Date()
           //"itcstatusdate":getTimeStamp(new Date())

     };
     
 
     mongodb.updateByQuery("itcdevices",{"itcSno":parseInt(req.body.S_ID)},objc,function(err,ldata){

       if(err)
       {
           logger.error(""+err.message);
         res.send({ "status":0,"message":"error in insertion of the record "+err.message});
       }
       else{
             

        mongodb.save('ITCdevicestatus',objc, function(err, result)
    {
            if(err)
            {
                 logger.error(""+err.message);
                res.send({"status":0,"message":"error in insertion of the status record"+err.message});
            }
            else
            {
               //res.json(result); 
               res.send({"status":1,"message":"updated status succesfully"})
            }

            
      }  );


       }


     });

    
    
});


  // changing the status of the device
  app.post('/updatestatus',function(req,res){

      updateobj={
        
          "itcStatus":req.body.itcStatus

      };

     mongodb.updateByQuery("itcdevices",{"itcSno":parseInt(req.body.itcSno)},updateobj,function(err,updata){
 
       if(err)
       {
              res.send({"status":0,"message":"error in retriving the record"});
       }
       else
       { 
           //res.json(updata);
           res.send({"status":1,"message":"status updated succesfully"});
       }


     });

   });
  
 // get lateststatus 

app.post('/getLastestStatus',function(req,res){
    
    //console.log(""+JSON.stringify(req.body));
    var limit=req.body.limit;
    mongodb.findSortedObjects('ITCdevicestatus',{"sid":parseInt(req.body.S_ID)},{itcstatusdate:-1},limit,function(err,edata)
     {

         if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

              if(edata!=null)
              {
               res.json(edata); 
             
              }
              else
              {
                   res.send({"status":0,"message":"no data found of given serial number please try again with valid serialnumber"});
              }
            

         }


     } );
   


});


// get data of ploting data


app.post('/plotgraph',function(req,res){
    
    //console.log(""+JSON.stringify(req.body));
  var query;

     if(req.body.isWeek=== 1)
     {
         var currentdate=new Date(req.body.fromDate);//.addDays(1);
         var oneweekdate=new Date(req.body.fromDate).addDays(7);
          //logger.error(oneweekdate);
          query={"sid":parseInt(req.body.S_ID),"itcstatusdate":{"$gte":new Date(currentdate),"$lt":new Date(oneweekdate)}};

     }
     else{

         if(req.body.fromDate != null || req.body.fromDate != undefined)
         {
            logger.error("comming here "+req.body.fromDate);
           // var currentdate1=new Date(req.body.fromDate).addDays(1);
            var oneweekdate1=new Date(req.body.fromDate).addDays(2);
           // logger.error("next here"+oneweekdate1);
            query={"sid":parseInt(req.body.S_ID),"itcstatusdate":{"$gte":new Date(req.body.fromDate),"$lt":new Date(oneweekdate1)}};
            //logger.info(JSON.stringify(query));
         }
         else{
             
             query={"sid":parseInt(req.body.S_ID)};
         }
     }  
    
    mongodb.findsortingObjects('ITCdevicestatus',query,{itcstatusdate:1},function(err,edata)
     {

         if(err)
         {
             res.send({"status":0,"message":"error in retriving the record"});
         }
         else{

              if(edata!=null)
              {
                
                   /*_.each(edata, function(v){
                       v.itcstatusdate=getTimeStamp(v.itcstatusdate);
                    });*/
                  
               res.json(edata); 
              }
              else
              {
                   res.send({"status":0,"message":"no data found of given serial number please try again with valid serialnumber"});
              }
            

         }


     } );
   


});

var getTimeStamp = function(date) {
    // date = "2016-03-05T16:25:10.250Z";
    return [ ("00"+(date.getMonth()+1)).slice(-2), ("00"+date.getDate()).slice(-2), ("00"+date.getFullYear()).slice(-4) ].join('/') + ' ' +
    [ ("00"+date.getHours()).slice(-2), ("00"+date.getMinutes()).slice(-2), ("00"+date.getSeconds()).slice(-2) ].join(':');
};

Date.prototype.addDays = function(days) {
 var dat = new Date(this.valueOf());
 dat.setDate(dat.getDate() + days);
 return dat;
}
Date.prototype.minusDays = function(days) {
 var dat = new Date(this.valueOf());
 dat.setDate(dat.getDate() - days);
 return dat;
}

function scheduler(){

  mongodb.findByObjects("itcdevices",{},function(err,res){
   
        if(err)
        {
           logger.info("problem in retriving the data " ) ;
        }
        else
        {
          var itcSnos=_.pluck(res,'itcSno');
          if(itcSnos[0])
          {
              var itssArr, obj = [], _date = new Date();
               _.each(res, function(v){
                obj.push({
                itc_id: v.itcSno,
                itcStatus: ((new Date(v.itcstatusdate)).getTime() < (_date.getTime() - Globals.graceTime)) ? "InActive" : "Active",
                timestamp: _date
                   });
               });
               itssArr=_.pluck(obj, "itc_id");
               logger.error("data formed"+itssArr);
               mongodb.findSorted('itc_on_off_status', {itc_id: {$in: itssArr}}, {timestamp: -1}, function(err, data){

                   if(!err)
                   {
                        if(data[0])
                        {

                            var _newObj = {};
                            _.each(data, function(v){

                                
                            if(!_newObj[v.itc_id]){
                                _newObj[v.itc_id] = v;
                                var _idx = itssArr.indexOf(v.itc_id);
                                // logger.info(obj[_idx].itcStatus);
                                // logger.info(v.itcStatus);
                                if(v.itcStatus == obj[_idx].itcStatus){
                                obj.splice(_idx, 1);
                                itssArr.splice(_idx, 1);
                                }
                            }
                            });

                        }
                        // else
                        // {
                        //     mongodb.save('itc_on_off_status', obj, function(){
                        //         setTimeout(scheduler, Globals.graceTime);
                        //         });
                        // }

                        if(obj[0]){
                                mongodb.save('itc_on_off_status', obj, function(){
                                setTimeout(scheduler, Globals.graceTime);
                                });
                            }else{
                                setTimeout(scheduler, Globals.graceTime);
                            }
                        

                   }

               });
               

          }
          else
          {
              logger.error("no devices found");
             // setTimeout(scheduler, Globals.graceTime);
          }
        }
   

  });
}



// function scheduler1(){
//   var Models = db.getModels();
//   Models.Hub.findAll({order: 'hub_id DESC'})
//   .then(function(hubs){
//     if(hubs[0]){
//       var hubsArr, obj = [], _date = new Date();
//       _.each(hubs, function(v){
//         obj.push({
//           hub_id: v.hub_id,
//           status: ((new Date(v.hub_status_date)).getTime() < (_date.getTime() - Globals.graceTime)) ? 0 : 1,
//           timestamp: _date
//         });
//       });
//       hubsArr = _.pluck(obj, "hub_id");
//       mongodb.findSortedObjects('hub_on_off_status', {hub_id: {$in: hubsArr}}, {timestamp: -1}, function(err, data){
//         if(!err){
//           if(data[0]){
//             var _newObj = {};
//             _.each(data, function(v){
//               if(!_newObj[v.hub_id]){
//                 _newObj[v.hub_id] = v;
//                 var _idx = hubsArr.indexOf(v.hub_id);
//                 if(v.status == obj[_idx].status){
//                   obj.splice(_idx, 1);
//                   hubsArr.splice(_idx, 1);
//                 }
//               }
//             });
//           }
//           if(obj[0]){
//             mongodb.save('hub_on_off_status', obj, function(){
//               setTimeout(scheduler, Globals.graceTime);
//             });
//           }else{
//             setTimeout(scheduler, Globals.graceTime);
//           }
//         }else{
//           logger.error("error in scheduler: " + err.message);
//           setTimeout(scheduler, Globals.graceTime);
//         }
//       });
//     }
//   });
// }



 
















